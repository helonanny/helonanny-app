package com.helonanny.common.events.division;

import java.util.UUID;

public class DivisionCreated extends DivisionInfoEvent {

	private UUID companyId;
	
	public DivisionCreated(UUID divisionId, UUID companyId, String divisionName) {
		super(divisionId, divisionName, null);
		this.companyId = companyId;
	}

	public UUID getCompanyId() {
		return companyId;
	}
}
