package com.helonanny.common.events.company;

import java.time.LocalDate;
import java.util.UUID;

import com.helonanny.common.vocab.company.CompanyType;

public class CompanyCreated extends CompanyInfoEvent {

	private CompanyType companyType;

	private LocalDate fromDate;

	public CompanyCreated() {
	}

	public CompanyCreated(UUID companyId, CompanyType companyType, String companyName, LocalDate fromDate) {
		super(companyId, companyName);
		this.fromDate = fromDate;
		this.fromDate = fromDate;
	}

	public CompanyType getCompanyType() {
		return companyType;
	}

	public LocalDate getFromDate() {
		return fromDate;
	}
}
