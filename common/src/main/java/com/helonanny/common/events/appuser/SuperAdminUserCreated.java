package com.helonanny.common.events.appuser;

import java.time.LocalDate;
import java.util.UUID;

public class SuperAdminUserCreated extends AppUserCreationEvent {

	public SuperAdminUserCreated() {
	}

	public SuperAdminUserCreated(UUID userId, String email, LocalDate fromDate) {
		super(userId, email, fromDate);
	}

}
