package com.helonanny.common.events.division;

import java.util.Set;
import java.util.UUID;

public class AssociatedCentresModified extends DivisionEvent {

	private Set<UUID> newCentres;

	private Set<UUID> originalCentres;

	public AssociatedCentresModified(UUID divisionId, Set<UUID> newCentres, Set<UUID> originalCentres) {
		super(divisionId);
		this.newCentres = newCentres;
		this.originalCentres = originalCentres;
	}

	public Set<UUID> getNewCentres() {
		return newCentres;
	}
	
	public Set<UUID> getOriginalCentres() {
		return originalCentres;
	}
}
