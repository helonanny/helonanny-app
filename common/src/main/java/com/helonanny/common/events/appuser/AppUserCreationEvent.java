package com.helonanny.common.events.appuser;

import java.time.LocalDate;
import java.util.UUID;

public class AppUserCreationEvent extends AppUserEvent {

	private String email;

	private LocalDate fromDate;

	public AppUserCreationEvent() {
	}

	public AppUserCreationEvent(UUID userId, String email, LocalDate fromDate) {
		super(userId);
		this.email = email;
		this.fromDate = fromDate;
	}

	public String getEmail() {
		return email;
	}

	public LocalDate getFromDate() {
		return fromDate;
	}
}
