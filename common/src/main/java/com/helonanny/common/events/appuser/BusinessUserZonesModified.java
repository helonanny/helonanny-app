package com.helonanny.common.events.appuser;

import java.util.Map;
import java.util.Set;
import java.util.UUID;

public class BusinessUserZonesModified extends AppUserEvent {

	private Map<UUID, Boolean> newZones;

	private Set<UUID> originalZones;

	public BusinessUserZonesModified() {
	}

	public BusinessUserZonesModified(UUID userId, Map<UUID, Boolean> newZones, Set<UUID> originalZones) {
		super(userId);
		this.newZones = newZones;
		this.originalZones = originalZones;
	}

	public Map<UUID, Boolean> getNewZones() {
		return newZones;
	}

	public Set<UUID> getOriginalZones() {
		return originalZones;
	}

}
