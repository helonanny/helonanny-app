package com.helonanny.common.events.centre;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.helonanny.common.events.Event;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY)
@JsonSubTypes({ @JsonSubTypes.Type(value = CentreCreated.class, name = "CentreCreated"),
		@JsonSubTypes.Type(value = CentreInfoModified.class, name = "CentreInfoModified") })
public abstract class CenterEvent extends Event {

	private UUID centreId;
	
	public CenterEvent() {
	}
	
	public CenterEvent(UUID centreId) {
		this.centreId = centreId;
	}
	
	public UUID getCentreId() {
		return centreId;
	}
}
