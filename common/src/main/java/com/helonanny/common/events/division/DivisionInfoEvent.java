package com.helonanny.common.events.division;

import java.util.UUID;

public abstract class DivisionInfoEvent extends DivisionEvent {

	private String newDivisionName;

	private String originalDivisionName;

	public DivisionInfoEvent() {
	}

	public DivisionInfoEvent(UUID divisionId, String newDivisionName, String originalDivisionName) {
		super(divisionId);
		this.newDivisionName = newDivisionName;
		this.originalDivisionName = originalDivisionName;
	}

	public String getNewDivisionName() {
		return newDivisionName;
	}

	public String getOriginalDivisionName() {
		return originalDivisionName;
	}
}
