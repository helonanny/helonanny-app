package com.helonanny.common.events;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.helonanny.common.events.appuser.AppUserEvent;
import com.helonanny.common.events.centre.CenterEvent;
import com.helonanny.common.events.company.CompanyEvent;
import com.helonanny.common.events.division.DivisionEvent;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY)
@JsonSubTypes({ @JsonSubTypes.Type(value = AppUserEvent.class, name = "AppUserEvent"),
		@JsonSubTypes.Type(value = CenterEvent.class, name = "CenterEvent"),
		@JsonSubTypes.Type(value = CompanyEvent.class, name = "CompanyEvent"),
		@JsonSubTypes.Type(value = DivisionEvent.class, name = "DivisionEvent") })
public abstract class Event {

}
