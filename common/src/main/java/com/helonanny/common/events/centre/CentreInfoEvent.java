package com.helonanny.common.events.centre;

import java.util.UUID;

public abstract class CentreInfoEvent extends CenterEvent {

	private String newDescription;

	private String originalDescription;

	public CentreInfoEvent() {
	}

	public CentreInfoEvent(UUID centreId, String newDescription, String originalDescription) {
		super(centreId);
		this.newDescription = newDescription;
		this.originalDescription = originalDescription;
	}

	public String getNewDescription() {
		return newDescription;
	}
	

	public String getOriginalDescription() {
		return originalDescription;
	}
}
