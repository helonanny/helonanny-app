package com.helonanny.common.events.division;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.helonanny.common.events.Event;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY)
@JsonSubTypes({ @JsonSubTypes.Type(value = DivisionCreated.class, name = "DivisionCreated"),
		@JsonSubTypes.Type(value = DivisionInfoModified.class, name = "DivisionInfoModified") })
public abstract class DivisionEvent extends Event {

	private UUID divisionId;
	
	public DivisionEvent() {
	}
	
	public DivisionEvent(UUID divisionId) {
		this.divisionId = divisionId;
	}
	
	public UUID getDivisionId() {
		return divisionId;
	}
}


