package com.helonanny.common.events.appuser;

import java.util.Set;
import java.util.UUID;

import com.helonanny.common.vocab.appuser.Permission;

public class BusinessUserPermissionsModified extends AppUserEvent {

	private Set<Permission> newPermissions;

	private Set<Permission> originalPermissions;

	public BusinessUserPermissionsModified() {
	}

	public BusinessUserPermissionsModified(UUID userId, Set<Permission> newPermissions,
			Set<Permission> originalPermissions) {
		super(userId);
		this.newPermissions = newPermissions;
		this.originalPermissions = originalPermissions;
	}

	public Set<Permission> getNewPermissions() {
		return newPermissions;
	}

	public Set<Permission> getOriginalPermissions() {
		return originalPermissions;
	}

}
