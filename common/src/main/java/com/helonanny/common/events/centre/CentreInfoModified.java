package com.helonanny.common.events.centre;

import java.util.UUID;

public class CentreInfoModified extends CentreInfoEvent {

	public CentreInfoModified() {
	}

	public CentreInfoModified(UUID centreId, String newDescription, String originalDescription) {
		super(centreId, newDescription, originalDescription);
	}

}
