package com.helonanny.common.events.appuser;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.helonanny.common.events.Event;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY)
@JsonSubTypes({ @JsonSubTypes.Type(value = SuperAdminUserCreated.class, name = "AdminUserCreated"),
		@JsonSubTypes.Type(value = BusinessUserCreated.class, name = "BusinessUserCreated"),
		@JsonSubTypes.Type(value = BusinessUserPermissionsModified.class, name = "OrderCreated") })
public abstract class AppUserEvent extends Event {

	private UUID userId;
	
	public AppUserEvent() {
	}
	
	public AppUserEvent(UUID userId) {
		this.userId = userId;
	}
	
	public UUID getUserId() {
		return userId;
	}
}
