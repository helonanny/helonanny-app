package com.helonanny.common.events.company;

import java.util.UUID;

public abstract class CompanyInfoEvent extends CompanyEvent {

	private String companyName;

	public CompanyInfoEvent() {
	}

	public CompanyInfoEvent(UUID companyId, String companyName) {
		super(companyId);
		this.companyName = companyName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
}
