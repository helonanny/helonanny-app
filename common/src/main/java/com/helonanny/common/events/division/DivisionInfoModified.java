package com.helonanny.common.events.division;

import java.util.UUID;

public class DivisionInfoModified extends DivisionInfoEvent {

	public DivisionInfoModified() {
	}

	public DivisionInfoModified(UUID divisionId, String newDivisionName, String originalDivisionName) {
		super(divisionId, newDivisionName, originalDivisionName);
	}

}
