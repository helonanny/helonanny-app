package com.helonanny.common.events.company;

import java.util.UUID;

public class CompanyInfoModified extends CompanyInfoEvent {

	public CompanyInfoModified() {
	}

	public CompanyInfoModified(UUID companyId, String companyName) {
		super(companyId, companyName);
	}

}
