package com.helonanny.common.events.centre;

import java.time.LocalDate;
import java.util.UUID;

import com.helonanny.common.vocab.centre.CentreType;

public class CentreCreated extends CentreInfoEvent {

	private UUID companyId;
	
	private CentreType centreType;

	private LocalDate fromDate;

	public CentreCreated() {
	}

	public CentreCreated(UUID centreId, UUID companyId, CentreType centreType, String description, LocalDate fromDate) {
		super(centreId, description, null);
		this.companyId = companyId;
		this.centreType = centreType;
		this.fromDate = fromDate;
	}

	public UUID getCompanyId() {
		return companyId;
	}
	

	public CentreType getCentreType() {
		return centreType;
	}

	public LocalDate getFromDate() {
		return fromDate;
	}
}
