package com.helonanny.common.events.company;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.helonanny.common.events.Event;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY)
@JsonSubTypes({ @JsonSubTypes.Type(value = CompanyCreated.class, name = "CompanyCreated"),
		@JsonSubTypes.Type(value = CompanyInfoModified.class, name = "CompanyInfoModified") })
public abstract class CompanyEvent extends Event {

	private UUID companyId;
	
	public CompanyEvent() {
	}
	
	public CompanyEvent(UUID companyId) {
		this.companyId = companyId;
	}
	
	public UUID getCompanyId() {
		return companyId;
	}
}


