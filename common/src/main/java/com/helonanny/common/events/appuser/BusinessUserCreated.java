package com.helonanny.common.events.appuser;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class BusinessUserCreated extends AppUserCreationEvent {

	private UUID initialZone;
	
	private boolean cascadeAccess;
	
	private Set<String> permissions = new HashSet<>();

	public BusinessUserCreated() {
	}

	public BusinessUserCreated(UUID userId, String email, UUID initialZone, boolean cascadeAccess, Set<String> permissions,
			LocalDate fromDate) {
		super(userId, email, fromDate);
		this.initialZone = initialZone;
		this.permissions = permissions;
		this.cascadeAccess = cascadeAccess;
	}

	public UUID getInitialZone() {
		return initialZone;
	}


	public boolean getCascadeAccess() {
		return cascadeAccess;
	}
	
	public Set<String> getPermissions() {
		return permissions;
	}

}
