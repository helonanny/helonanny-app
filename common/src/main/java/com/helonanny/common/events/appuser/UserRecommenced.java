package com.helonanny.common.events.appuser;

import java.util.UUID;

public class UserRecommenced extends AppUserEvent {

	public UserRecommenced(UUID userId) {
		super(userId);
	}

}
