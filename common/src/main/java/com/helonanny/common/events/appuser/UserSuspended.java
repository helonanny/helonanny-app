package com.helonanny.common.events.appuser;

import java.util.UUID;

public class UserSuspended extends AppUserEvent {

	public UserSuspended(UUID userId) {
		super(userId);
	}

}
