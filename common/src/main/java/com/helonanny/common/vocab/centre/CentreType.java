package com.helonanny.common.vocab.centre;

public enum CentreType {
	LICENSED_CHILD_CARE_CENTRE, LICENSED_HOME_CHILD_CARE, PRIVATE_HOME_CHILD_CARE
}
