package com.helonanny.common.vocab.appuser;

import static com.helonanny.common.vocab.appuser.Behaviour.ADMIN;
import static com.helonanny.common.vocab.appuser.Behaviour.DIRECTOR;

import javax.persistence.Embeddable;

@Embeddable
public enum Permission {

	APPUSER_CREATE(ADMIN), APPUSER_READ(ADMIN), APPUSER_MODIFY_ZONES(ADMIN), APPUSER_MODIFY_PERMISSIONS(ADMIN),
	
	COMPANY_CREATE(ADMIN), COMPANY_READ(ADMIN, DIRECTOR), COMPANY_MODIFY_INFO(ADMIN, DIRECTOR);
	
	private Behaviour[] behaviours;
	
	Permission(Behaviour ...behaviours) {
		this.behaviours = behaviours;
	}
	
}
