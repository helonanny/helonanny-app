package com.helonanny.common.vocab.company;

public enum CompanyType {
	LICENSED_CHILD_CARE_COMPANY, LICENSING_AGENCY, PRIVATE_HOME_CHILD_CARE_COMPANY
}
