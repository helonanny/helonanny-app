package com.helonanny.common.vocab.appuser;

public enum Behaviour {
	ADMIN, DIRECTOR, SUPERVISOR, TEACHER, GUARDIAN;

}