# HeloNanny


https://github.com/sdaschner/scalable-coffee-shop

http://www.baeldung.com/spring-vertx

http://danielwhittaker.me/

## appuser
https://spring.io/guides/tutorials/spring-boot-oauth2/
https://medium.com/@benjaminliu_42474/how-to-setup-spring-boot-with-reactjs-and-webpack-9b190edeb0c8


# h2

http://localhost:8080/h2-console

JDBC URL: jdbc:h2:mem:testdb
user: sa
password:

# docker-confluent

##Run:

Add line
      127.0.0.1       kafka_broker_1
      127.0.0.1       kafka_broker_2
      127.0.0.1       kafka_broker_3
to /etc/hosts

> cd .../microservices-course
> docker-compose up -d

##Verify:
> docker-compose ps

##ZooKeeper health:
> docker-compose logs zookeeper | grep -i binding

##Kafka health:
> docker-compose logs kafka | grep -i started

##Create Topic:
> docker-compose exec broker kafka-topics --create --topic page_visits --partitions 1 --replication-factor 1 --if-not-exists --zookeeper zookeeper:2181

##Verify Topic creation:
> docker-compose exec broker  kafka-topics --describe --topic page_visits --zookeeper zookeeper:2181

##Verify Topic creation:
path-to-confluent>./bin/kafka-avro-console-producer \
         --broker-list 127.0.0.1:9092 --topic test \
         --property value.schema='{"type":"record","name":"myrecord","fields":[{"name":"f1","type":"string"}]}'
         
{"f1": "value1"}
{"f1": "value2"}
{"f1": "value3"}         

path-to-confluent>./bin/kafka-avro-console-consumer --topic test \
         --bootstrap-server 127.0.0.1:2181 \
         --from-beginning

##Stop:
> docker-compose down  --remove-orphans



##Ref

###Kafka client examples
https://github.com/confluentinc/examples/tree/3.3.0-post/kafka-clients

