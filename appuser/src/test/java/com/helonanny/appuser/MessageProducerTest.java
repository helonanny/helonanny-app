package com.helonanny.appuser;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.helonanny.common.events.Event;
import com.helonanny.common.events.EventProducer;
import com.helonanny.common.events.ExtrinsicEventConsumer;
import com.helonanny.common.events.IntrinsicEventConsumer;
import com.helonanny.common.events.appuser.BusinessUserCreated;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class MessageProducerTest {

	@MockBean
	IntrinsicEventConsumer intrinsicEventConsumer;

	@MockBean
	EventProducer intrinsicEventProducer;

	@MockBean
	ExtrinsicEventConsumer extrinsicEventConsumer;

	@Captor
	ArgumentCaptor<Event> eventCaptor;

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void testOne() throws Exception {
		restTemplate.postForEntity(
				"/appuser/businessuser?email=Bob@google.com&initialZone=2fcbcf32-6923-11e8-adc0-fa7ae01bbebc&cascadeAccess=true&permission=APPUSER_CREATE",
				null, Void.class);

		Mockito.verify(intrinsicEventProducer).publish(eventCaptor.capture());
		assertEquals("Bob@google.com", ((BusinessUserCreated) eventCaptor.getValue()).getEmail());
	}
}
