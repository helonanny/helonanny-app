package com.helonanny.appuser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.net.URI;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.inject.Inject;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.google.common.collect.Sets;
import com.google.common.eventbus.EventBus;
import com.helonanny.appuser.domain.AccessZone;
import com.helonanny.appuser.domain.AppUser;
import com.helonanny.appuser.domain.BusinessUser;
import com.helonanny.appuser.domain.SuperAdminUser;
import com.helonanny.appuser.events.ZoneCreated;
import com.helonanny.common.events.EventProducer;
import com.helonanny.common.events.ExtrinsicEventConsumer;
import com.helonanny.common.events.IntrinsicEventConsumer;
import com.helonanny.common.events.appuser.BusinessUserCreated;
import com.helonanny.common.events.appuser.BusinessUserZonesModified;
import com.helonanny.common.events.appuser.SuperAdminUserCreated;
import com.helonanny.common.vocab.appuser.Permission;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class MessageConsumerTest {

	@LocalServerPort
	private int port;

	private URI uri;

	@Inject
	private ClientConfig config;

	@MockBean
	private IntrinsicEventConsumer intrinsicEventConsumer;

	@MockBean
	private EventProducer intrinsicEventProducer;

	@MockBean
	private ExtrinsicEventConsumer extrinsicEventProducer;

	@Inject
	private EventBus eventBus;

	@Before
	public void setUp() throws Exception {
		uri = new URI("http://localhost:" + port);
		eventBus.post(new ZoneCreated(UUID.fromString("e4d284f0-2545-4368-ae80-8278c33edf15")));
		eventBus.post(new ZoneCreated(UUID.fromString("e4d284f0-2545-4368-ae80-8278c33edf16")));
		eventBus.post(new ZoneCreated(UUID.fromString("e4d284f0-2545-4368-ae80-8278c33edf16")));
		eventBus.post(new SuperAdminUserCreated(UUID.fromString("e4d284f0-2545-4368-ae80-8278c33edf17"),
				"BobSuperAdmin@google.com", LocalDate.now()));
		eventBus.post(new SuperAdminUserCreated(UUID.fromString("e4d284f0-2545-4368-ae80-8278c33edf18"),
				"JohnSuperAdmin@google.com", LocalDate.now()));
		eventBus.post(new BusinessUserCreated(UUID.fromString("e4d284f0-2545-4368-ae80-8278c33edf19"),
				"BillAdmin@google.com", UUID.fromString("e4d284f0-2545-4368-ae80-8278c33edf16"), true,
				Sets.newHashSet(Permission.APPUSER_READ.name(), Permission.APPUSER_CREATE.name()), LocalDate.now()));
		eventBus.post(new BusinessUserCreated(UUID.fromString("e4d284f0-2545-4368-ae80-8278c33edf20"),
				"DanDirector@google.com", UUID.fromString("e4d284f0-2545-4368-ae80-8278c33edf15"), true,
				Sets.newHashSet(Permission.COMPANY_READ.name(), Permission.COMPANY_MODIFY_INFO.name()),
				LocalDate.now()));

		Map<UUID, Boolean> newZones = new HashMap<>();
		newZones.put(UUID.fromString("e4d284f0-2545-4368-ae80-8278c33edf15"), true);
		newZones.put(UUID.fromString("e4d284f0-2545-4368-ae80-8278c33edf16"), false);
		eventBus.post(new BusinessUserZonesModified(UUID.fromString("e4d284f0-2545-4368-ae80-8278c33edf20"), newZones,
				Sets.newHashSet(UUID.fromString("e4d284f0-2545-4368-ae80-8278c33edf15"))));
	}

	@Test
	public void getAllAppUsers_listOfUsers() throws Exception {
		Response response = ClientBuilder.newClient(config).target(uri).path("appuser").path("list")
				.request(MediaType.APPLICATION_JSON).get();
		List<AppUser> appUsers = response.readEntity(new GenericType<List<AppUser>>() {
		});

		assertEquals("Number of appUsers is wrong,", 4, appUsers.size());
		for (AppUser appUser : appUsers) {
			switch (appUser.getUserId().toString()) {
			case "e4d284f0-2545-4368-ae80-8278c33edf17":
				assertEquals("BobSuperAdmin@google.com", ((SuperAdminUser) appUser).getEmail());
				break;
			case "e4d284f0-2545-4368-ae80-8278c33edf18":
				assertEquals("JohnSuperAdmin@google.com", ((SuperAdminUser) appUser).getEmail());
				break;
			case "e4d284f0-2545-4368-ae80-8278c33edf19":
				assertEquals("BillAdmin@google.com", ((BusinessUser) appUser).getEmail());
				assertEquals(Sets.newHashSet(Permission.APPUSER_READ.name(), Permission.APPUSER_CREATE.name()),
						((BusinessUser) appUser).getPermissions());
				break;
			case "e4d284f0-2545-4368-ae80-8278c33edf20":
				assertEquals("DanDirector@google.com", ((BusinessUser) appUser).getEmail());
				assertEquals(Sets.newHashSet(Permission.COMPANY_READ.name(), Permission.COMPANY_MODIFY_INFO.name()),
						((BusinessUser) appUser).getPermissions());
				break;
			default:
				assertFalse("appUser not found", true);
				break;
			}
		}
	}

	@Test
	public void getAppUser_superAdminUser() throws Exception {
		Response response = ClientBuilder.newClient(config).target(uri).path("appuser")
				.path("e4d284f0-2545-4368-ae80-8278c33edf17").request(MediaType.APPLICATION_JSON).get();
		SuperAdminUser appUser = response.readEntity(SuperAdminUser.class);
		assertEquals("e4d284f0-2545-4368-ae80-8278c33edf17", appUser.getUserId().toString());
		assertEquals("BobSuperAdmin@google.com", appUser.getEmail());
		assertEquals(LocalDate.now(), appUser.getFromDate());
	}

	@Test
	public void getAppUser_businessUser() throws Exception {
		Response response = ClientBuilder.newClient(config).target(uri).path("appuser")
				.path("e4d284f0-2545-4368-ae80-8278c33edf19").request(MediaType.APPLICATION_JSON).get();
		BusinessUser appUser = response.readEntity(BusinessUser.class);
		assertEquals("e4d284f0-2545-4368-ae80-8278c33edf19", appUser.getUserId().toString());
		assertEquals("BillAdmin@google.com", appUser.getEmail());
		assertEquals(Sets.newHashSet(Permission.APPUSER_READ.name(), Permission.APPUSER_CREATE.name()),
				((BusinessUser) appUser).getPermissions());
		assertEquals("e4d284f0-2545-4368-ae80-8278c33edf16",
				appUser.getAccessZones().toArray(new AccessZone[0])[0].getZone().getZoneId().toString());
		assertEquals(LocalDate.now(), appUser.getFromDate());
	}

	@Test
	public void getAppUser_businessUserWithMyltyZone() throws Exception {
		Response response = ClientBuilder.newClient(config).target(uri).path("appuser")
				.path("e4d284f0-2545-4368-ae80-8278c33edf20").request(MediaType.APPLICATION_JSON).get();
		BusinessUser appUser = response.readEntity(BusinessUser.class);
		assertEquals("e4d284f0-2545-4368-ae80-8278c33edf20", appUser.getUserId().toString());
		assertEquals("DanDirector@google.com", appUser.getEmail());
		assertEquals(Sets.newHashSet(Permission.COMPANY_READ.name(), Permission.COMPANY_MODIFY_INFO.name()),
				((BusinessUser) appUser).getPermissions());
		Set<String> zoneIds = new HashSet<>();
		for (AccessZone accessZone : appUser.getAccessZones()) {
			zoneIds.add(accessZone.getZone().getZoneId().toString() + "-" + accessZone.isCascade());
		}
		assertEquals(Sets.newHashSet("e4d284f0-2545-4368-ae80-8278c33edf15-true",
				"e4d284f0-2545-4368-ae80-8278c33edf16-false"), zoneIds);
	}

}
