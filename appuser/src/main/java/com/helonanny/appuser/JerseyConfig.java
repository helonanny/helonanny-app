package com.helonanny.appuser;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

import com.helonanny.appuser.rest.AppUserResource;

@Configuration
public class JerseyConfig extends ResourceConfig {

	public JerseyConfig() {
		register(AppUserResource.class);
	}

}
