package com.helonanny.appuser;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication(scanBasePackageClasses = { com.helonanny.RootMarker.class })
public class AppUserApp extends SpringBootServletInitializer {

	public static void main(String[] args) {
		new AppUserApp().configure(new SpringApplicationBuilder(AppUserApp.class)).run(args);
	}

}
