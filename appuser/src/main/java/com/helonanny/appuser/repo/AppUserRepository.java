package com.helonanny.appuser.repo;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

import com.helonanny.appuser.domain.AppUser;

public interface AppUserRepository extends CrudRepository<AppUser, UUID> {

}
