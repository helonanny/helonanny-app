package com.helonanny.appuser.repo;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

import com.helonanny.appuser.domain.Zone;

public interface ZoneRepository extends CrudRepository<Zone, UUID> {

}
