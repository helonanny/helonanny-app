package com.helonanny.appuser.rest;

import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.springframework.stereotype.Component;

import com.helonanny.appuser.domain.AppUser;
import com.helonanny.appuser.service.AppUserCommandService;
import com.helonanny.appuser.service.AppUserQueryService;

@Component
@Path("/appuser")
public class AppUserResource {

	@Inject
	private AppUserQueryService queryService;

	@Inject
	private AppUserCommandService commandService;

	@GET
	@Path("/list")
	@Produces(MediaType.APPLICATION_JSON)
	public List<AppUser> getAllAppUsers() {
		return queryService.getAllAppUsers();
	}

	@GET
	@Path("/{userId}")
	@Produces(MediaType.APPLICATION_JSON)
	public AppUser getAppUser(@PathParam("userId") @NotNull UUID userId) throws Exception {
		return queryService.getAppUser(userId);
	}

	@POST
	@Path("/businessuser")
	public void createBusinessUser(@QueryParam("email") String email, //
			@QueryParam("initialZone") UUID initialZone, //
			@QueryParam("cascadeAccess") Boolean cascadeAccess, //
			@QueryParam("permission") Set<String> permissions) {
		commandService.createBusinessUser(email, initialZone, cascadeAccess, permissions);
	}

	@POST
	@Path("/superadminuser")
	public void createSuperAdminUser(@QueryParam("email") String email) {
		commandService.createSuperAdminUser(email);
	}

}
