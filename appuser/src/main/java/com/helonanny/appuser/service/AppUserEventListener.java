package com.helonanny.appuser.service;

import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.helonanny.appuser.domain.AccessZone;
import com.helonanny.appuser.domain.BusinessUser;
import com.helonanny.appuser.domain.SuperAdminUser;
import com.helonanny.appuser.domain.Zone;
import com.helonanny.appuser.events.ZoneAssociationsModified;
import com.helonanny.appuser.events.ZoneCreated;
import com.helonanny.appuser.repo.AppUserRepository;
import com.helonanny.appuser.repo.ZoneRepository;
import com.helonanny.common.events.EventProducer;
import com.helonanny.common.events.appuser.BusinessUserCreated;
import com.helonanny.common.events.appuser.BusinessUserZonesModified;
import com.helonanny.common.events.appuser.SuperAdminUserCreated;
import com.helonanny.common.events.centre.CentreCreated;
import com.helonanny.common.events.division.AssociatedCentresModified;
import com.helonanny.common.events.division.DivisionCreated;

@Named
public class AppUserEventListener {

	@Inject
	Logger logger;

	@Inject
	EventBus eventBus;

	@Inject
	AppUserRepository appUserRepository;

	@Inject
	ZoneRepository zoneRepository;

	@Inject
	EventProducer eventProducer;

	@PostConstruct
	public void init() {
		eventBus.register(this);
	}

	@Subscribe
	public void apply(SuperAdminUserCreated event) {
		appUserRepository.save(new SuperAdminUser(event.getUserId(), event.getEmail(), event.getFromDate()));
	}

	@Subscribe
	public void apply(BusinessUserCreated event) {
		appUserRepository.save(new BusinessUser(event.getUserId(), event.getEmail(), event.getInitialZone(),
				event.getCascadeAccess(), event.getPermissions(), event.getFromDate()));
	}

	@Subscribe
	public void apply(ZoneCreated event) {
		zoneRepository.save(new Zone(event.getZoneId()));
	}
	
	@Subscribe
	public void apply(BusinessUserZonesModified event) {
		BusinessUser businessUser = (BusinessUser)appUserRepository.findById(event.getUserId()).get();
		Set<AccessZone> accessZones = new HashSet<>();
		for (Entry<UUID, Boolean> entry : event.getNewZones().entrySet()) {
			accessZones.add(new AccessZone(businessUser, new Zone(entry.getKey()), entry.getValue()));
		}
		businessUser.setAccessZones(accessZones);
		appUserRepository.save(businessUser);
	}

	@Subscribe
	public void apply(DivisionCreated event) {
		eventProducer.publish(new ZoneCreated(event.getDivisionId()));
	}

	@Subscribe
	public void apply(CentreCreated event) {
		eventProducer.publish(new ZoneCreated(event.getCentreId()));
	}

	@Subscribe
	public void apply(AssociatedCentresModified event) {
		eventProducer.publish(new ZoneAssociationsModified(event.getDivisionId(), event.getNewCentres(), event.getOriginalCentres()));
	}	

}
