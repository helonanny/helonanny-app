package com.helonanny.appuser.service;

import java.time.LocalDate;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.inject.Inject;
import javax.inject.Named;

import com.helonanny.common.events.EventProducer;
import com.helonanny.common.events.appuser.BusinessUserCreated;
import com.helonanny.common.events.appuser.BusinessUserPermissionsModified;
import com.helonanny.common.events.appuser.BusinessUserZonesModified;
import com.helonanny.common.events.appuser.SuperAdminUserCreated;
import com.helonanny.common.events.appuser.UserRecommenced;
import com.helonanny.common.events.appuser.UserSuspended;
import com.helonanny.common.vocab.appuser.Permission;

@Named
public class AppUserCommandService {

	@Inject
	EventProducer eventProducer;

	public void createSuperAdminUser(String email) {
		eventProducer.publish(new SuperAdminUserCreated(UUID.randomUUID(), email, LocalDate.now()));
	}

	public void createBusinessUser(String email, UUID initialZone, boolean cascadeAccess, Set<String> permissions) {
		eventProducer.publish(new BusinessUserCreated(UUID.randomUUID(), email, initialZone, cascadeAccess, permissions,
				LocalDate.now()));
	}

	public void suspendUser(UUID userId) {
		eventProducer.publish(new UserSuspended(userId));
	}

	public void recommenceUser(UUID userId) {
		eventProducer.publish(new UserRecommenced(userId));
	}

	public void modifyBusinessUserPermissions(UUID userId, Set<Permission> newPermissions,
			Set<Permission> originalPermissions) {
		eventProducer.publish(new BusinessUserPermissionsModified(userId, newPermissions, originalPermissions));
	}

	public void modifyBusinessUserZones(UUID userId, Map<UUID, Boolean> newZones, Set<UUID> originalZones) {
		eventProducer.publish(new BusinessUserZonesModified(userId, newZones, originalZones));
	}
}
