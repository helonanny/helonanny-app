package com.helonanny.appuser.service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.NotNull;

import com.helonanny.appuser.domain.AppUser;
import com.helonanny.appuser.repo.AppUserRepository;

@Named
public class AppUserQueryService {

	@Inject
	AppUserRepository userRepository;

	public AppUser getAppUser(@NotNull UUID userId) {
		return userRepository.findById(userId).get();
	}

	public List<AppUser> getAllAppUsers() {
		List<AppUser> users = new ArrayList<>();
		userRepository.findAll().forEach(users::add);
		return users;
	}

}
