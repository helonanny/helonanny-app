package com.helonanny.appuser.events;

import java.util.UUID;

public class ZoneCreated extends ZoneEvent {

	public ZoneCreated(UUID zoneId) {
		super(zoneId);
	}

}
