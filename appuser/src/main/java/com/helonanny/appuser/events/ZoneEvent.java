package com.helonanny.appuser.events;

import java.util.UUID;

import com.helonanny.common.events.Event;

public class ZoneEvent extends Event {

	private UUID zoneId;

	public ZoneEvent(UUID zoneId) {
		this.zoneId = zoneId;
	}

	public UUID getZoneId() {
		return zoneId;
	}
}
