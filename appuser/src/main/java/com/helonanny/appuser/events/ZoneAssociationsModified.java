package com.helonanny.appuser.events;

import java.util.Set;
import java.util.UUID;

public class ZoneAssociationsModified extends ZoneEvent {

	private Set<UUID> newAssociations;
	
	private Set<UUID> originalAssociations;

	public ZoneAssociationsModified(UUID zoneId, Set<UUID> newAssociations, Set<UUID> originalAssociations) {
		super(zoneId);
		this.newAssociations = newAssociations;
		this.originalAssociations = originalAssociations;
	}

	public Set<UUID> getNewAssociations() {
		return newAssociations;
	}

	public Set<UUID> getOriginalAssociations() {
		return originalAssociations;
	}

}
