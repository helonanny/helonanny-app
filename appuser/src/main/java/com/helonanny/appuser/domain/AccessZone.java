package com.helonanny.appuser.domain;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class AccessZone {

	@Id
	@GeneratedValue
	private long id;

	@ManyToOne(fetch = FetchType.LAZY)
	private BusinessUser businessUser;

	@ManyToOne
	private Zone zone;

	private boolean cascade;

	public AccessZone() {
	}

	public AccessZone(BusinessUser businessUser, Zone zone, boolean cascade) {
		this.businessUser  = businessUser;
		this.zone = zone;
		this.cascade = cascade;
	}

	public Zone getZone() {
		return zone;
	}

	public void setZone(Zone zone) {
		this.zone = zone;
	}

	public boolean isCascade() {
		return cascade;
	}

	public void setCascade(boolean cascade) {
		this.cascade = cascade;
	}

}
