package com.helonanny.appuser.domain;

import java.time.LocalDate;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY)
@JsonSubTypes({ @JsonSubTypes.Type(value = BusinessUser.class, name = "BusinessUser"),
		@JsonSubTypes.Type(value = SuperAdminUser.class, name = "SuperAdminUser") })
public abstract class AppUser {

	@Id
	private UUID userId;

	private String email;

	private LocalDate fromDate;

	public AppUser() {
	}

	public AppUser(UUID userId, String email, LocalDate fromDate) {
		this.userId = userId;
		this.email = email;
		this.fromDate = fromDate;
	}

	public UUID getUserId() {
		return userId;
	}

	public String getEmail() {
		return email;
	}

	public LocalDate getFromDate() {
		return fromDate;
	}

	@Override
	public String toString() {
		return getUserId().toString();
	}
}
