package com.helonanny.appuser.domain;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

@Entity
public class BusinessUser extends AppUser {

	@ElementCollection(fetch = FetchType.EAGER)
	private Set<String> permissions = new HashSet<>();

	@OneToMany(mappedBy = "businessUser", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private Set<AccessZone> accessZones;

	public BusinessUser() {
	}

	public BusinessUser(UUID userId, String email, UUID initialZone, boolean cascadeAccess, Set<String> permissions,
			LocalDate fromDate) {
		super(userId, email, fromDate);
		accessZones = new HashSet<>();
		accessZones.add(new AccessZone(this, new Zone(initialZone), cascadeAccess));
		this.permissions = permissions;
	}

	public Set<String> getPermissions() {
		return permissions;
	}

	public Set<AccessZone> getAccessZones() {
		return accessZones;
	}

	public void setAccessZones(Set<AccessZone> accessZones) {
		this.accessZones = accessZones;
	}
}
