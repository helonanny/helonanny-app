package com.helonanny.appuser.domain;

import java.time.LocalDate;
import java.util.UUID;

import javax.persistence.Entity;

@Entity
public class SuperAdminUser extends AppUser {

	public SuperAdminUser() {
	}

	public SuperAdminUser(UUID userId, String email, LocalDate fromDate) {
		super(userId, email, fromDate);
	}

}
