package com.helonanny.appuser.domain;

import java.util.Set;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * Zones hierarchy represents a directed acyclic graph - a finite directed graph
 * with no directed cycles.
 * 
 * TODO - verify zones graph for cycles on any update.
 * 
 * @author michaellif
 *
 */
@Entity
public class Zone {

	@Id
	private UUID zoneId;

	private String description;

	@OneToMany(fetch = FetchType.EAGER)
	private Set<Zone> children;

	public Zone() {
	}

	public Zone(UUID zoneId) {
		this.zoneId = zoneId;
	}

	public Zone(UUID zoneId, String description, Set<Zone> children) {
		this.zoneId = zoneId;
		this.description = description;
		this.children = children;
	}

	public UUID getZoneId() {
		return zoneId;
	}

	public String getDescription() {
		return description;
	}

	public Set<Zone> getChildren() {
		return children;
	}
}
