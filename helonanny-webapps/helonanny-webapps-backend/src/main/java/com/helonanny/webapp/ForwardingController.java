package com.helonanny.webapp;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ForwardingController {

	@RequestMapping("/**/{path:[^\\.]+}")
	public String forward() {
		return "forward:/";
	}

	@RequestMapping({ "/login", "/register" })
	public String login() {
		return "forward:/signin.html";
	}

	@RequestMapping({ "/enroll" })
	public String companyEnroll() {
		return "forward:/company-enroll.html";
	}
	
	@RequestMapping({ "/admin" })
	public String admin() {
		return "forward:/admin.html";
	}
}