package com.helonanny.webapp;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		if (!registry.hasMappingForPattern("/helonenny-signin/**")) {
			registry.addResourceHandler("/helonenny-signin/**").addResourceLocations("classpath:/META-INF/resources/webjars/helonenny-signin/");
		}
		
		if (!registry.hasMappingForPattern("/helonenny-company-enroll/**")) {
			registry.addResourceHandler("/helonenny-company-enroll/**").addResourceLocations("classpath:/META-INF/resources/webjars/helonenny-company-enroll/");
		}
		
		if (!registry.hasMappingForPattern("/helonenny-admin/**")) {
			registry.addResourceHandler("/helonenny-admin/**").addResourceLocations("classpath:/META-INF/resources/webjars/helonenny-admin/");
		}
	}
}
