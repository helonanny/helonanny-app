package com.helonanny.webapp;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

import com.helonanny.webapp.rest.HealthResource;
import com.helonanny.webapp.rest.MockResource;


@Configuration
@ApplicationPath("/webapi")
public class JerseyConfig extends ResourceConfig {
    public JerseyConfig() {
        super();

        register(HealthResource.class);
        register(MockResource.class);

    }
}
