package com.helonanny.webapp.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.stereotype.Component;

@Component
@Path("/mock")
public class MockResource {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getAllMocks() {
		return "Test";
	}


}
