const packageJSON = require('./package.json');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');


const PATHS = {
	build : path.join(__dirname, 'target', 'classes', 'META-INF', 'resources',
		'webjars', packageJSON.name)
};

module.exports = {
	entry : './src/app/index.jsx',
	output : {
		path : PATHS.build,
		filename : 'bundle.js'
	},
	resolve : {
		extensions : [ '.js', '.jsx' ]
	},
	module : {
		loaders : [
			{
				test : /\.jsx?$/,
				include : __dirname + "/src/",
				loader : 'babel-loader',
				query : {
					presets : [ 'react', 'es2015', 'stage-3' ]
				}
			},

			{
				test : /\.(jpe?g|png|gif|svg|css)$/,
				include : __dirname + "/src/",
				loader : 'file-loader'
			}
		]
	},
	plugins : [
		new CopyWebpackPlugin([
			// Copy directory contents to {output}/to/directory/
			{
				from : __dirname + "/src/public",
				to : PATHS.build + '/public'
			}, {
				from : __dirname + "/src/index.html",
				to : PATHS.build + '/'
			} ], {
			ignore : [
				// Doesn't copy any files with a txt extension
				// '*.txt',

				// Doesn't copy any file, even if they start with a dot
				// '**/*',
			]
		}),
		new HtmlWebpackPlugin({
			template : './src/app/index.html',
			filename : 'index.html',
			inject : 'body'
		}) ],
	devServer : {
		historyApiFallback : true
	},
	externals : {
		// global app config object
		config : JSON.stringify({
			apiUrl : 'http://localhost:4000'
		})
	}
}