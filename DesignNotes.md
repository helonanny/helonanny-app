## Modules

### User (Authentication,  Authorization, SSO, Roles, Permissions)
### Company (Region, Centers, Employees)
### Center (Provider, Profile, Policies, Visits, Daily Reports)
### Account (Guardian, Student, Medical, Attendance)
### Lead
### Billing
### Communication

### Scheduler
- Schedule request consumer receives schedule event requests from other components and places it to schedule topic.
- Single instance Scheduler consumes schedule topic and fills in memory db.
- Single instance Scheduler triggers event at scheduled moment and produces event into trigger topic.
- Trigger topic message is consumed by other components (use key for directing specific trigger events to the appropriate consumer)

### Employee



## Reference 

### Spring security project for authorization with Facebook, Google ,LinkedIn and Twitter using OAUTH2
https://github.com/abhishek-galoda/spring-oauth2-facebook-google-linkedIn-twitter

### Best practice for REST token-based authentication with JAX-RS and Jersey
https://stackoverflow.com/questions/26777083/best-practice-for-rest-token-based-authentication-with-jax-rs-and-jersey

### Apps
Procare,Daycare Works, EZ-Care,Oncare,SchoolLeader, ChildcareManager, lifecubby

### Persistent cluster-friendly scheduler for Java
https://github.com/kagkarlsson/db-scheduler

### Childcare software manuals
http://www.childcaremanager.com/media/53694/ChildcareManagerUsersGuide.pdf
http://www.childcaresage.com/pdfmanuals/ChildcareSage_Manual.pdf
https://www.softerware.com/doclib/EZ-CARE2/Manuals/CARE2_SUP_Manual_V70.pdf
http://cdn.procaresoftware.com/pdfs/support/Procare-User-Guide.pdf
