package com.helonanny.customer.service;

import java.time.LocalDate;
import java.util.UUID;

import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.NotNull;

import com.helonanny.common.events.EventProducer;
import com.helonanny.common.events.company.ClubProgram;
import com.helonanny.common.events.company.ClubProgramAssigned;
import com.helonanny.common.events.company.CompanyCreated;
import com.helonanny.common.events.company.CompanyInfoModified;

@Named
public class CustomerCommandService {

	@Inject
	EventProducer eventProducer;

	public void createCustomer(String firstName, String lastName) {
		eventProducer.publish(new CompanyCreated(UUID.randomUUID(), firstName, lastName, LocalDate.now()));

	}

	public void modifyCustomer(UUID customerId, String firstName, String lastName) {
		eventProducer.publish(new CompanyInfoModified(customerId, firstName, lastName));
	}

	public void assignClubProgram(@NotNull UUID customerId, ClubProgram clubProgram) {
		eventProducer.publish(new ClubProgramAssigned(customerId, clubProgram));
	}

}
