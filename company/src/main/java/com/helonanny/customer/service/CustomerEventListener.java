package com.helonanny.customer.service;

import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.helonanny.common.events.company.CompanyCreated;
import com.helonanny.common.events.company.CompanyInfoModified;
import com.helonanny.customer.domain.Customer;
import com.helonanny.customer.repo.CustomerRepository;

@Named
public class CustomerEventListener {

	@Inject
	Logger logger;

	@Inject
	EventBus eventBus;

	@Inject
	CustomerRepository customerRepository;

	@PostConstruct
	public void init() {
		eventBus.register(this);
	}

	@Subscribe
	public void apply(CompanyCreated event) {
		customerRepository.save(
				new Customer(event.getCustomerId(), event.getFirstName(), event.getLastName(), event.getFromDate()));
	}

	@Subscribe
	public void apply(CompanyInfoModified event) {
		Customer customer = customerRepository.findById(event.getCustomerId()).get();
		customer.setFirstName(event.getFirstName());
		customer.setLastName(event.getLastName());
		customerRepository.save(customer);
	}

}
