package com.helonanny.order.repo;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

import com.helonanny.order.domain.Customer;

public interface CustomerRepository extends CrudRepository<Customer, UUID> {

}
