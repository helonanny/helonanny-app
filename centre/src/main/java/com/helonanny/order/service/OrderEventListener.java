package com.helonanny.order.service;

import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.helonanny.common.events.centre.CentreCreated;
import com.helonanny.common.events.company.CompanyCreated;
import com.helonanny.common.events.company.CompanyInfoModified;
import com.helonanny.order.domain.Customer;
import com.helonanny.order.domain.Order;
import com.helonanny.order.repo.CustomerRepository;
import com.helonanny.order.repo.OrderRepository;

@Named
public class OrderEventListener {

	@Inject
	Logger logger;

	@Inject
	EventBus eventBus;

	@Inject
	OrderRepository orderRepository;

	@Inject
	CustomerRepository customerRepository;

	@PostConstruct
	public void init() {
		eventBus.register(this);
	}

	@Subscribe
	public void apply(CompanyCreated event) {
		customerRepository.save(new Customer(event.getCustomerId(), event.getFirstName(), event.getLastName()));
	}

	@Subscribe
	public void apply(CompanyInfoModified event) {
		Customer customer = customerRepository.findById(event.getCustomerId()).get();
		customer.setFirstName(event.getFirstName());
		customer.setLastName(event.getLastName());
		customerRepository.save(customer);
	}

	@Subscribe
	public void apply(CentreCreated event) {
		orderRepository.save(new Order(event.getOrderId(), customerRepository.findById(event.getCustomerId()).get(),
				event.getDescription(), event.getCreationDate()));
	}

}
