package com.helonanny.order.service;

import java.util.Date;
import java.util.UUID;

import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.NotNull;

import com.helonanny.common.events.EventProducer;
import com.helonanny.common.events.centre.CentreCreated;
import com.helonanny.common.events.centre.CentreInfoModified;
import com.helonanny.common.vocab.centre.CentreType;
import com.helonanny.order.repo.OrderRepository;

@Named
public class OrderCommandService {

	@Inject
	EventProducer eventProducer;

	@Inject
	OrderRepository orderRepository;

	public void createOrder(UUID customerId, String description) {
		eventProducer.publish(new CentreCreated(UUID.randomUUID(), customerId, description, new Date()));
	}

	public void setOrderStatus(@NotNull UUID orderId, CentreType newOrderStatus) {
		eventProducer.publish(new CentreInfoModified(UUID.randomUUID(), newOrderStatus,
				orderRepository.findById(orderId).get().getOrderStatus()));
	}

}
