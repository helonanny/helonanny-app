package com.helonanny.order;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.helonanny.common.events.Event;
import com.helonanny.common.events.ExtrinsicEventConsumer;
import com.helonanny.common.events.IntrinsicEventConsumer;
import com.helonanny.common.events.EventProducer;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class MessageProducerTest {

	@MockBean
	IntrinsicEventConsumer intrinsicEventConsumer;

	@MockBean
	EventProducer intrinsicEventProducer;

	@MockBean
	ExtrinsicEventConsumer extrinsicEventProducer;

	@Captor
	ArgumentCaptor<Event> eventCaptor;

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void testOne() throws Exception {
		// restTemplate.postForEntity("/customer?firstName=Bob&lastName=Poe", null,
		// Void.class);
		//
		// Mockito.verify(intrinsicEventProducer).publish(eventCaptor.capture());
		// assertEquals("Bob", ((CustomerCreated)
		// eventCaptor.getValue()).getFirstName());
	}
}
